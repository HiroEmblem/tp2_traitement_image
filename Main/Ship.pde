class Ship extends GraphicObject {
  
  float topSpeed = 5;
  float NbLaser = 0;
  ArrayList<Laser> laser; 
  float angle=0;
  float r = 10;
  float fillColor = 150;
  float head = 0;
  float mass = 3;
  boolean shootOnce = false;
  int time = 0;
  int wait = 100;
  
  
  
  Ship () {
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector();
    
    laser = new ArrayList<Laser>();
  }
  
  Ship (PVector loc) {
    this.location = loc;
    velocity = new PVector();
    acceleration = new PVector();
    
    laser = new ArrayList<Laser>();
  }
  
  void update(float deltaTime) {
    checkEdges();
    ifkeyPressed();
    
    velocity.add (acceleration);
    location.add (velocity); 
    velocity.limit(topSpeed);
    
    acceleration.mult (0);
    for (Laser l : laser) 
    {
      if(l != null)
    l.update(deltaTime); 
    }
  }
  
  void checkEdges() {
    if (location.x < 0) {
      location.x = width - r;
    } else if (location.x + r> width) {
      location.x = 0;
    }
    
    if (location.y < 0) {
      location.y = height - r;
    } else if (location.y + r> height) {
      location.y = 0;
    }
  }
  
  public boolean IsColliding(Boids boid)
  {
   
    if((this.location.x + r > boid.location.x - boid.r) && (this.location.x < boid.location.x + boid.r) && (this.location.y + r > boid.location.y - boid.r) && (this.location.y < boid.location.y + boid.r))
        return true;
    
     return false;
  }  

  
  float getangle()
  {
    return angle;
  }
  
  void display() {
    noStroke();
    fill(fillColor);
    
    pushMatrix();
      translate(location.x,location.y);
      rotate(head);
     
      beginShape(TRIANGLES);
        vertex(0, -r * 2);
        vertex(-r, r * 2);
        vertex(r, r * 2);
      endShape();
    popMatrix();
     for (Laser l : laser) 
     {
       if(l!= null)
       l.display(); 
     }
  }
  
  void turn(float turnvalue)
  {
    head +=turnvalue;
  }
  
  void ifkeyPressed()
  {
    if(keyPressed)
    {
      if(key == 'd')
      {
        turn(0.1);
      }
      else if(key == 'a')
      {
        turn(-0.1);
      }
      else if(key == 'w')
      {
        float angle = head - PI/2;
        PVector force = PVector.fromAngle(angle);
        force.mult(0.2);
        applyForce(force); 
    
        force.mult(-2);
      }
      else if(key == ' ')
      {
     
      if(NbLaser < 5)
       {
        if (millis()>= time+wait) { 
    
         Laser l= new Laser(new PVector(location.x,location.y),head,ship.location);
         float angle = head - PI/2;
         PVector Fire =  PVector.fromAngle(angle);
         l.applyForce(Fire);
         laser.add(l);
         NbLaser +=1;
         time = millis();
      } 
    }
    else
    {
     for (int i =0; i< laser.size(); i++)
     {
      if(laser.get(i) != null)
      {
       if(laser.get(i).checkLaserEdges()== true)
       {
        laser.remove(i);
        NbLaser --;
       }
      }
     }
    }  
       //Relâche un nouveau laser 
      } 
    }
  }
   void applyForce (PVector force) {
    PVector f = PVector.div(force,mass);  
    this.acceleration.add(f);
  }
}
