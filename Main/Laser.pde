class Laser extends GraphicObject {
  float angle = 90;
  float fillColor = 100;
  float speed = 5;
  PVector location2;
  Laser() 
  {
     this.location = new PVector();
     this.velocity = new PVector();
     this.acceleration = new PVector();
  }
 
  Laser(PVector loc, float angle,PVector shipLoc) 
  {
    this.location = loc;
    this.velocity = new PVector (0, 0);
    this.acceleration = new PVector (0 , 0);
    this.angle = angle;
    float NewAngle = angle - PI/2;
    location2 = new PVector((shipLoc.x + cos( NewAngle  )*7),(shipLoc.y + sin( NewAngle  )*7));
  }
  
  boolean checkLaserEdges() {
    if (location.x < 0) {
      return true;
    } else if (location.x> width) {
      return true;
    }
    if (location.y < 0) {
      return true;
    } else if (location.y> height) {
      return true;
    }
    return false;
  }

   public boolean IsColliding(Boids boid)
  {
   
    if((this.location.x >=  boid.location.x - boid.r ) && (this.location.x < boid.location.x + boid.r)&&(this.location.y >= boid.location.y - boid.r) && (this.location.y < boid.location.y + boid.r))
        return true;
    if((this.location2.x >=  boid.location.x - boid.r ) && (this.location2.x < boid.location.x + boid.r)&&(this.location2.y >= boid.location.y - boid.r) && (this.location2.y < boid.location.y + boid.r))    
      return true;
      
     return false;
  }  
 
  void update(float deltaTime) 
  {
    velocity.add (acceleration);
    location.add (velocity); 
    location2.add(velocity);
     
    acceleration.mult (0);
}
 
  void display() {
   stroke(0);
   strokeWeight(3); 
  pushMatrix(); 
    line(location.x, location.y, location2.x,location2.y); 
  popMatrix();
  }
  
  void applyForce (PVector force) {
    PVector f = PVector.mult(force,speed);  
    this.acceleration.add(f);
  }
  
}
