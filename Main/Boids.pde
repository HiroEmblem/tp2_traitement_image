class Boids extends GraphicObject {  
  float r = 7; // Rayon du boid
  PVector sum;
  float topSpeed = 5;
  float topSteer = 0.03;
  float radiusSeparation = 10 * r;
  float radiusAlignment = 20 * r;
  float radiusCohesion = 30 * r;

  float weightSeparation = 2;
  float weightAlignment = 1;
  float weightCohesion = 2;
  float mass = 1;
  float theta = 0;
  PVector sumCohesion;
  PVector steer;
  
   PVector zeroVector = new PVector(0, 0);
  Boids () {
   location = new PVector();
   this.velocity = new PVector (0, 0);
   this.acceleration = new PVector (0 , 0);
  }
  
  Boids (PVector loc,  PVector vel) {
    this.location = loc;
    this.velocity = vel;
    this.acceleration = new PVector (0 , 0);
  }
  
  void flock (ArrayList<Boids> boids) {
    PVector separation = separate(boids);
    PVector alignment = align(boids);
    PVector cohesion = cohesion(boids);
    
    separation.mult(weightSeparation);
    alignment.mult(weightSeparation);
    cohesion.mult(weightCohesion);

    applyForce(separation);
    applyForce(alignment);
    applyForce(cohesion);
  }
  
    void checkEdges() {
    if (location.x < 0) {
      location.x = width - r;
    } else if (location.x + r> width) {
      location.x = 0;
    }
    
    if (location.y < 0) {
      location.y = height - r;
    } else if (location.y + r> height) {
      location.y = 0;
    }
  }
  
  
  void update(float deltaTime) {
    checkEdges();
    
    velocity.add (acceleration);

    velocity.limit(topSpeed);
    
    theta = velocity.heading() + radians(90);
    
    location.add (velocity);

    acceleration.mult (0);  
     }
     
     PVector align (ArrayList<Boids> boids) 
     {
      if (sum == null) 
          sum = new PVector();      
       else
          sum.mult(0);
       
       int count = 0;

       for (Boids other : boids) 
       {
         float d = PVector.dist(this.location, other.location);
          if (d > 0 && d < radiusAlignment) 
          {
            sum.add(other.velocity);
            count++;
          }
       }

    if (count > 0) {
      sum.div((float)count);
      sum.setMag(topSpeed);
      PVector steer = PVector.sub(sum, this.velocity);
      steer.limit(topSteer);

      return steer;
    } else {
      return new PVector();
    }
      
    
  }

  void display() {
    noStroke();
    fill (fillColor);
   
    pushMatrix();
      translate(location.x, location.y);
       rotate (theta);
       
      beginShape(TRIANGLES);
      vertex(0, -r * 2);
      vertex(-r, r * 2);
      vertex(r, r * 2);
    endShape();
     popMatrix();
  }
  
   void applyForce (PVector force) {
    PVector f;
    
    if (mass != 1)
      f = PVector.div (force, mass);
    else
      f = force;
   
    this.acceleration.add(f);    
  }
  PVector separate (ArrayList<Boids> boids) {
    if (steer == null) {
      steer = new PVector(0, 0, 0);
    }
    else {
      steer.setMag(0);
    }
    
    int count = 0;
    
    for (Boids other : boids) {
      float d = PVector.dist(location, other.location);
      
      if (d > 0 && d < radiusSeparation) {
        PVector diff = PVector.sub(location, other.location);
        
        diff.normalize();
        diff.div(d);
        
        steer.add(diff);
        
        count++;
      }
    }
    
    if (count > 0) {
      steer.div(count);
    }
    
    if (steer.mag() > 0) {
      steer.setMag(topSpeed);
      steer.sub(velocity);
      steer.limit(topSteer);
    }
    
    return steer;
  }
  
  PVector seek (PVector target) {
    // Vecteur différentiel vers la cible
    PVector desired = PVector.sub (target, this.location);
    
    // VITESSE MAXIMALE VERS LA CIBLE
    desired.setMag(topSpeed);
    
    // Braquage
    PVector steer = PVector.sub (desired, velocity);
    steer.limit(topSteer);
    
    return steer;    
  }
  PVector cohesion (ArrayList<Boids> boids) {
    if (sumCohesion == null) {
      sumCohesion = new PVector();      
    } else {
      sumCohesion.mult(0);
    }

    int count = 0;

    for (Boids other : boids) {
      float d = PVector.dist(location, other.location);

      if (d > 0 && d < radiusCohesion) {
        sumCohesion.add(other.location);
        count++;
      }
    }

    if (count > 0) {
      sumCohesion.div(count);

      return seek(sumCohesion);
    } else {
      return zeroVector;
    }
    
  }
 }
