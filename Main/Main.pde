int currentTime;
int previousTime;
int deltaTime;
float shipX;
float shipY;
float NbBoids = 50;
float ColorWhite = 255;
Ship ship;  
ArrayList<Boids> boids; 
int score = 0;
int scoreMax =50;
double OneThirdDiagonal= (Math.sqrt(((300*300) + 400*400)))/5;
float AddPos = (float)OneThirdDiagonal;

void setup () {
  size (800, 600);
  currentTime = millis();
  previousTime = millis();
  score = 0;
  float shipX = width/2;
  float shipY = height/2;
  ship = new Ship(new PVector(shipX,shipY));
  boids = new ArrayList<Boids>();
  //float BoidspositionX = random(600,width);
  //float BoidspositionY = random(400,height);
  for (int i = 0; i < NbBoids; i++) {
    Boids b = new Boids(new PVector(AddPos,AddPos),new PVector(random (-2, 2), random(-2, 2)));
    b.fillColor = color(random(255), random(255), random(255));
    boids.add(b);
  }
}

void draw () {
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;

  update();
  display();  
}

/***
  The calculations should go here
*/
void update() {
    if(ship != null)
    ship.update(deltaTime);
    
    for (int j = 0; j < boids.size();j++) 
  {
    if((boids.get(j)!=null)&&(ship!=null))
    {
    boids.get(j).display();
     for (int i =0; i< ship.laser.size(); i++)
     {
       if(ship.laser != null)
       if(ship.laser.get(i).IsColliding(boids.get(j)))
       {
        boids.remove(j);
        ship.laser.remove(i);
        ship.NbLaser--;
        score++;
        break;
       }
     }
    }
  }
    
    for (Boids b : boids) 
    {
      if(b != null)
      {
      if(ship != null)
      {
        if(ship.IsColliding(b))
        {
          ship = null;
        }
      }
      b.flock(boids);
      b.update(deltaTime);
     }
  }
}
  


void keyPressed() {
  if (key == 'r') 
  { 
      setup();   
  }
}



/***
  The rendering should go here
*/
void display () {
  background(ColorWhite);
  
  if(ship!=null)
  ship.display(); 
  else
  {
    fill(50);
    textSize(32);
    text("Destroyed", (width/2)-100, height/2); 
  }
  
  if(score>=50)
  {
    fill(50);
    textSize(32);
    text("Win", (width/2)-100, height/2); 
  }
  fill(50);
  textSize(32);
  text(score+"/"+scoreMax, (width-width/8), (height/18)); 

  for (int j = 0; j < boids.size();j++) 
  {
    if(boids.get(j)!=null)
    {
       boids.get(j).display();
    }
   }
}
